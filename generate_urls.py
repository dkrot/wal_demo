#!/usr/bin/python3
import sys
import argparse

parser = argparse.ArgumentParser(description='Generate sequence for WAL test.')
parser.add_argument('nurls', metavar='N', type=int, help='number of URLs to add')
parser.add_argument('--loops', metavar='N', type=int, help='number of loops', default=1)

args = parser.parse_args()

def print_add(url):
    print("+ %s %s" % (url, url))

def print_del(url):
    print("- %s" % url)

def generate_url(i):
    return "http://site-%d/" % i


loop = 0
while loop < args.loops:
    print('One more loop (#%d)' % loop, file=sys.stderr)
    start = loop * args.nurls
    end = start + args.nurls

    i = start
    while i < end:
       print_add(generate_url(i))
       i += 1

    i = start + 100
    while i < end:
       print_del(generate_url(i))
       i += 1

    loop += 1
