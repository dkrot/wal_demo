# WAL demo

## Structure
- main/org.bytesoft.wal.impl.* - wal implementation code
- main/org.bytesoft.wal.* - wal wrapper, should be used
- main/org.bytesoft.demo.WalDemo - example of how to use WAL
- test/org.bytesoft.wal.impl.* - unit-tests for implementation parts
- ./generate_urls.py - script for testing it

## Building
```
$ ./gradlew installApp
$ ./gradlew test
```

## Example
Run demo and get WAL file compacted.
It will make WAL for 10 times for 1000000 urls.
And then compare recovered urls with expected;
```
$ ./full_test.sh
```
