package org.bytesoft.wal.impl;

import org.apache.commons.io.FileUtils;
import org.bytesoft.wal.WalConfig;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.Random;
import java.util.TreeSet;

import static org.junit.Assert.*;


public class WalTest {
    private static Path tmp_dir = Paths.get("/tmp/wal-test");
    private WalConfig cfg;

    private static void mkdirWithExceptions(File dir) throws IOException {
        if (!dir.exists() && !dir.mkdirs())
            throw new IOException("Can't create directory " + dir.getAbsolutePath());
    }

    @Before
    public void prepareTest() throws IOException {
        cfg = new WalConfig().withDirectory(tmp_dir);
        mkdirWithExceptions(tmp_dir.toFile());
        mkdirWithExceptions(cfg.getCompactingTmpDir());
    }

    @After
    public void rmTempDir() throws IOException {
        FileUtils.deleteDirectory(tmp_dir.toFile());
    }

    private File getTempFile() throws IOException {
        return File.createTempFile("wal-", ".test", tmp_dir.toFile());
    }

    private WalConfig getWalConfig(long max_mem) {
        return cfg.withMaxMemory(max_mem);
    }


    @Test
    public void TestWriteAndRead() throws IOException {
        File temp_file = getTempFile();

        try (WalWriter writer = new WalWriter(temp_file)) {
            writer.add("key1", "data1".getBytes());
            writer.add("key2", "data2".getBytes());
            writer.remove("key1");
        }

        try (WalReader reader = new WalReader(temp_file)) {
            assertTrue(reader.hasNext());
            {
                WalRecord rec = reader.next();
                assertTrue(rec.is_add);
                assertEquals("key1", rec.key);
                assertEquals("data1", new String(rec.data));
            }

            assertTrue(reader.hasNext());
            {
                WalRecord rec = reader.next();
                assertTrue(rec.is_add);
                assertEquals("key2", rec.key);
                assertEquals("data2", new String(rec.data));
            }

            assertTrue(reader.hasNext());
            {
                WalRecord rec = reader.next();
                assertFalse(rec.is_add);
                assertEquals("key1", rec.key);
            }

            assertFalse(reader.hasNext());
        }
    }

    @Test
    public void TestCompactMini() throws IOException {
        File temp_file = getTempFile();

        try (WalWriter writer = new WalWriter(temp_file)) {
            writer.add("key1", "data1".getBytes());
            writer.add("key2", "data2".getBytes());
            writer.remove("key1");
        }

        File compacted_file = getTempFile();
        WalCompactor compactor = new WalCompactor(getWalConfig(1));
        compactor.Compact(Collections.singletonList(temp_file), compacted_file);

        try (WalReader reader = new WalReader(compacted_file)) {
            assertTrue(reader.hasNext());
            {
                WalRecord rec = reader.next();
                assertTrue(rec.is_add);
                assertEquals("key2", rec.key);
                assertEquals("data2", new String(rec.data));
            }

            assertFalse(reader.hasNext());
        }
    }

    private String makeDataForKey(String key) {
        return "very long data for key " + key;
    }

    @Test
    public void TestCompactMedium() throws IOException {
        File temp_file = getTempFile();
        TreeSet<String> keys = new TreeSet<>();
        Random rand = new Random();

        try (WalWriter writer = new WalWriter(temp_file)) {
            for (int i = 0; i < 1000000; i++) {
                String key = String.format("key_%d", i);
                keys.add(key);
                writer.add(key, makeDataForKey(key).getBytes());

                if (rand.nextBoolean()) {
                    // time to time write removes
                    String rem_key = keys.iterator().next();
                    if (rem_key != null) {
                        writer.remove(rem_key);
                        keys.remove(rem_key);
                    }
                }
            }
        }


        File compacted_file = getTempFile();
        WalCompactor compactor = new WalCompactor(getWalConfig(10000 * 1024));
        compactor.Compact(Collections.singletonList(temp_file), compacted_file);

        try (WalReader reader = new WalReader(compacted_file)) {
            for (WalRecord rec : reader) {
                String expected_data = makeDataForKey(rec.key);
                assertEquals("wrong data for key (" + new String(rec.data) + ")", expected_data, new String(rec.data));
                assertTrue("we removed this key (" + rec.key + ")", keys.remove(rec.key));
            }
        }

        assertEquals("Some keys not found in WAL", 0, keys.size());
    }

    @Test
    public void ConfigTest() {
        WalConfig cfg = new WalConfig()
                .withMaxMemory(1024)
                .withCompactThreshold(0.5f, 100500);

        assertEquals(1024, cfg.getMaxMemory());

        String expected_path = Paths.get("./temp").toFile().getAbsolutePath();
        assertEquals(expected_path, cfg.getCompactingTmpDir().getAbsolutePath());
    }
}
