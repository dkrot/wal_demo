package org.bytesoft.demo;

import org.bytesoft.wal.WalConfig;
import org.bytesoft.wal.WalRecoverer;
import org.bytesoft.wal.WriteAheadLog;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.Scanner;

import static java.nio.charset.StandardCharsets.UTF_8;

public class WalDemo {
    private WalConfig cfg;

    private static void mkdirWithExceptions(File dir) throws IOException {
        if (!dir.exists() && !dir.mkdirs())
            throw new IOException("Can't create directory " + dir.getAbsolutePath());
    }

    private WalDemo() throws IOException {
        Path tmp_dir = Paths.get("/tmp/wal-demo");
        mkdirWithExceptions(tmp_dir.toFile());
        cfg = new WalConfig().withDirectory(tmp_dir);
    }

    public static void main(String[] args) throws Exception {
        WalDemo demo = new WalDemo();
        demo.recoverWal();
        demo.writeEvents();
    }

    private void writeEvents() throws IOException, InterruptedException {
        WriteAheadLog wal = new WriteAheadLog(cfg);

        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {
            String line = sc.nextLine();
            String[] parts = line.split("\\s+");

            switch (parts[0]) {
                case "+":
                    if (parts.length != 3)
                        throw new RuntimeException("format error: expected + key data");
                    wal.add(parts[1], parts[2].getBytes(UTF_8));
                    break;

                case "-":
                    if (parts.length != 2)
                        throw new RuntimeException("format error: expected - key");
                    wal.remove(parts[1]);
                    break;

                default:
                    throw new RuntimeException("format error: expected +/- as first field");
            }
        }

        if (!wal.forceCompact()) {
            wal.waitCompact();
            wal.forceCompact();
        }

        wal.waitCompact();
        wal.close();
    }

    private void recoverWal() throws IOException {
        WalRecoverer recoverer = new WalRecoverer(cfg);
        Iterator<byte[]> it_data = recoverer.recover();
        while (it_data.hasNext()) {
            System.out.println("RECOVER: \"" + new String(it_data.next(), UTF_8) + "\"");
        }

        WriteAheadLog.removeCompactedWal(cfg);
    }
}
