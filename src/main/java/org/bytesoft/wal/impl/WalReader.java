package org.bytesoft.wal.impl;

import java.io.*;
import java.util.Iterator;

import static java.nio.charset.StandardCharsets.UTF_8;

public class WalReader implements Iterator<WalRecord>, Closeable, AutoCloseable, Iterable<WalRecord> {
    private DataInputStream stream;
    private WalRecord cur_record = null;
    private expandable_buf vbuf = new expandable_buf();

    public WalReader(File f) throws IOException {
        stream = new DataInputStream(new BufferedInputStream(new FileInputStream(f)));
    }

    @Override
    public Iterator<WalRecord> iterator() {
        return this;
    }

    @Override
    public void close() throws IOException {
        stream.close();
        cur_record = null;
    }

    private boolean readRecord() throws IOException {
        cur_record = new WalRecord();

        try {
            cur_record.is_add = stream.readBoolean();
            int key_len = stream.readInt();

            vbuf.expand(key_len);
            stream.readFully(vbuf.buffer, 0, key_len);
            cur_record.key = new String(vbuf.buffer, 0, key_len, UTF_8);

            if (cur_record.is_add) {
                int val_len = stream.readInt();
                cur_record.data = new byte[val_len];
                stream.readFully(cur_record.data);
            }

            return true;
        } catch (EOFException e) {
            return false;
        }
    }

    @Override
    public boolean hasNext() throws RuntimeException {
        try {
            return readRecord();
        } catch (IOException e) {
            throw new RuntimeException("IOException: " + e.toString(), e.getCause());
        }
    }

    @Override
    public WalRecord next() {
        return cur_record;
    }

    private class expandable_buf {
        byte[] buffer = new byte[0];

        void expand(int size) {
            if (buffer.length < size)
                buffer = new byte[size];
        }
    }
}
