package org.bytesoft.wal.impl;

public class WalRecord {
    boolean is_add;
    String key;
    byte[] data;

    public byte[] GetData() {
        return data;
    }

    int estimateSize() {
        return 32 + key.length() + (data == null ? 0 : data.length);
    }
}
