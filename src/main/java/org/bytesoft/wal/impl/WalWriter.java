package org.bytesoft.wal.impl;

import java.io.*;

import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * Write ahead log needs as state storage for daemons.
 * For example, fetching daemon with queue. It stores URLs (with date) coming to daemon and
 * removes old records since they downloaded.
 */
public class WalWriter implements Flushable, Closeable, AutoCloseable {
    private DataOutputStream writer;

    public WalWriter(File file) throws IOException {
        writer = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(file)));
    }

    private int serializeKey(String key) throws IOException {
        byte[] bytes = key.getBytes(UTF_8);

        writer.writeInt(bytes.length);
        writer.write(bytes);
        return Integer.BYTES + key.length();
    }

    private int serializeData(byte[] data) throws IOException {
        writer.writeInt(data.length);
        writer.write(data);
        return Integer.BYTES + data.length;
    }

    public int add(String key, byte[] data) throws IOException {
        writer.writeBoolean(true);
        return 1 + serializeKey(key) + serializeData(data);
    }

    public int remove(String key) throws IOException {
        writer.writeBoolean(false);
        return 1 + serializeKey(key);
    }

    void writeRecord(WalRecord rec) throws IOException {
        if (rec.is_add)
            add(rec.key, rec.data);
        else
            remove(rec.key);
    }

    @Override
    public void flush() throws IOException {
        writer.flush();
    }

    @Override
    public void close() throws IOException {
        flush();
        writer.close();
    }
}
