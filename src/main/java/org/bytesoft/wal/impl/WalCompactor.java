package org.bytesoft.wal.impl;

import org.bytesoft.wal.WalConfig;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;

/**
 * WalCompactor - class performing compaction (optimisation of WAL)
 * <p>
 * Example is:
 * + key1, data1
 * + key2, data2
 * - key1, data1
 * <p>
 * Will be compacted to:
 * + key2, data2
 * <p>
 * Since files can be large, use external sorting
 */
public class WalCompactor {
    private WalConfig cfg;

    public WalCompactor(WalConfig cfg) {
        this.cfg = cfg;
    }

    private ArrayList<WalRecord> eliminatePairEvents(ArrayList<WalRecord> buf, boolean save_removes) {
        buf.sort(new RecordsComparator());
        WalRecord cur_remove = null;

        int iwr = -1; // we will compact array "in place"
        for (int i = 0; i < buf.size(); i++) {
            WalRecord rec = buf.get(i);

            if (rec.is_add) {
                boolean paired = (cur_remove != null && cur_remove.key.equals(rec.key));

                if (!paired) {
                    if (save_removes && cur_remove != null)
                        buf.set(++iwr, cur_remove);

                    buf.set(++iwr, rec);
                }

                cur_remove = null;
            } else {
                if (cur_remove != null && save_removes)
                    buf.set(++iwr, cur_remove);

                cur_remove = rec;
            }
        }

        if (cur_remove != null && save_removes)
            buf.set(++iwr, cur_remove);

        for (int i = buf.size() - 1; i > iwr; i--) {
            buf.remove(i);
        }
        buf.trimToSize();

        return buf;
    }

    private File makeTempFile(ArrayList<WalRecord> buf) throws IOException {
        File tf = File.createTempFile("sort-", ".tmp", cfg.getCompactingTmpDir());
        writeRecords(buf, tf);
        return tf;
    }

    private void writeRecords(ArrayList<WalRecord> buf, File out_file) throws IOException {
        try (WalWriter writer = new WalWriter(out_file)) {
            for (WalRecord rec : buf) {
                writer.writeRecord(rec);
            }
        }
    }

    private void mergeSortedFiles(List<File> files, File out_file) throws IOException {
        Comparator<WalRecord> cmp = new RecordsComparator();
        PriorityQueue<BinaryFileBuffer> pq = new PriorityQueue<>(
                (a, b) -> cmp.compare(a.peek(), b.peek())
        );

        for (File f : files) {
            BinaryFileBuffer bf = new BinaryFileBuffer(new WalReader(f));
            if (!bf.empty())
                pq.add(bf);
        }

        try (WalWriter writer = new WalWriter(out_file)) {
            String cur_remove = null;
            while (pq.size() > 0) {
                BinaryFileBuffer bf = pq.poll();
                WalRecord rec = bf.pop();

                if (bf.empty())
                    bf.close();
                else
                    pq.add(bf); // add this file again

                if (rec.is_add) {
                    if (cur_remove == null || !cur_remove.equals(rec.key))
                        writer.add(rec.key, rec.data);

                    cur_remove = null;
                } else {
                    cur_remove = rec.key;
                }
            }
        }
    }

    public void Compact(List<File> in_files, File out_file) throws IOException {
        ArrayList<File> tempfiles = new ArrayList<>();
        ArrayList<WalRecord> buf = new ArrayList<>();
        long buf_mem_size = 0;

        for (File f : in_files) {
            try (WalReader reader = new WalReader(f)) {
                while (reader.hasNext()) {
                    WalRecord rec = reader.next();
                    buf.add(rec);
                    buf_mem_size += rec.estimateSize();

                    if (buf_mem_size >= cfg.getMaxMemory()) {
                        tempfiles.add(makeTempFile(eliminatePairEvents(buf, true)));
                        buf.clear();
                        buf_mem_size = 0;
                    }
                }
            }
        }

        if (tempfiles.isEmpty()) {
            // we have all data in memory, so write final result right here
            writeRecords(eliminatePairEvents(buf, false), out_file);
        } else {
            if (!buf.isEmpty()) {
                tempfiles.add(makeTempFile(eliminatePairEvents(buf, true)));
                buf.clear();
            }
            mergeSortedFiles(tempfiles, out_file);
        }
    }

    static class RecordsComparator implements Comparator<WalRecord> {
        @Override
        public int compare(WalRecord a, WalRecord b) {
            int res = a.key.compareTo(b.key);

            if (res == 0) {
                if (a.is_add == b.is_add)
                    return 0;
                return a.is_add ? 1 : -1;
            }

            return res;
        }
    }

    /**
     * This is essentially a thin wrapper on top of a WalRecord... which keeps
     * the last record in memory.
     */
    final class BinaryFileBuffer {
        private WalReader reader;
        private WalRecord cache;

        BinaryFileBuffer(WalReader reader) throws IOException {
            this.reader = reader;
            reload();
        }

        void close() throws IOException {
            reader.close();
        }

        boolean empty() {
            return cache == null;
        }

        WalRecord peek() {
            return cache;
        }

        WalRecord pop() throws IOException {
            WalRecord rec = cache; // make a copy
            reload();
            return rec;
        }

        private void reload() throws IOException {
            cache = reader.hasNext() ? reader.next() : null;
        }
    }
}
