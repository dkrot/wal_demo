package org.bytesoft.wal;


import org.bytesoft.wal.impl.WalReader;

import java.io.IOException;
import java.util.Iterator;

/**
 * WalRecoverer need for reading WAL file at program launch.
 */
public class WalRecoverer {
    private final WalConfig cfg;

    public WalRecoverer(WalConfig cfg) {
        this.cfg = cfg;
    }

    public Iterator<byte[]> recover() throws IOException {
        WriteAheadLog.CompactWal(cfg, true);
        WalReader reader = new WalReader(cfg.getCompactedWalPath());
        return new Iterator<byte[]>() {
            @Override
            public boolean hasNext() {
                return reader.hasNext();
            }

            @Override
            public byte[] next() {
                return reader.next().GetData();
            }
        };
    }
}
