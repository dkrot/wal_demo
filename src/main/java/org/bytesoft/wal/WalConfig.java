package org.bytesoft.wal;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;


public class WalConfig {
    float compact_threshold_ratio = 0.75f;
    long compact_threshold_bytes = 128 * 1024 * 1024;
    private Path dir;
    private long max_memory = 64 * 1024 * 1024;

    public WalConfig() {
        dir = Paths.get(".");
    }

    File getCurrentWalPath() {
        return dir.resolve("current.wal").toFile();
    }

    File getPreviousWalPath() {
        return dir.resolve("prev.wal").toFile();
    }

    File getCompactedWalPath() {
        return dir.resolve("compacted.wal").toFile();
    }

    File getCompactedTmpWalPath() {
        return dir.resolve("compacted.wal.tmp").toFile();
    }

    public File getCompactingTmpDir() {
        return dir.resolve("temp").toFile();
    }

    public long getMaxMemory() {
        return max_memory;
    }

    /**
     * Set directory for files of WAL
     *
     * @param dir directory (should exist)
     * @return config for chaining settings
     */
    public WalConfig withDirectory(Path dir) {
        this.dir = dir;
        return this;
    }

    /**
     * Set threshold for launching background compact thread
     *
     * @param threshold_ratio ratio of removed/added
     * @param threshold_bytes number of bytes in WAL-file
     * @return config for chaining settings
     */
    public WalConfig withCompactThreshold(float threshold_ratio, long threshold_bytes) {
        compact_threshold_ratio = threshold_ratio;
        compact_threshold_bytes = threshold_bytes;
        return this;
    }

    /**
     * Set maximal memory to use in compaction.
     *
     * @param max_memory memory in bytes.
     * @return config for chaining settings
     */
    public WalConfig withMaxMemory(long max_memory) {
        this.max_memory = max_memory;
        return this;
    }
}
