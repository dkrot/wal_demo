package org.bytesoft.wal;

import org.apache.commons.io.FileUtils;
import org.bytesoft.wal.impl.WalCompactor;
import org.bytesoft.wal.impl.WalWriter;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;


public class WriteAheadLog implements Closeable {
    private final WalConfig cfg;
    private WalWriter writer;
    private statistics stat = new statistics();
    private CompactorThread cthread = null;
    private volatile boolean compact_running = false;

    /**
     * Construct WriteAheadLog with given config.
     * This call will truncate current WAL, so should be used only after recovering
     *
     * @param cfg config with directory
     * @throws IOException if failed to create current.wal
     */
    public WriteAheadLog(WalConfig cfg) throws IOException {
        this.cfg = cfg;
        reopenWal();
    }

    static private void removeWithException(File f) throws IOException {
        if (f.exists() && !f.delete())
            throw new IOException("Can't remove file: " + f.getAbsolutePath());
    }

    static private void moveWithException(File from, File to) throws IOException {
        if (!from.renameTo(to))
            throw new IOException("Can't rename " +
                    from.getAbsolutePath() + " to " + to.getAbsolutePath());
    }

    /**
     * Compact (rotated) WAL.
     * Files:
     * - compacted (previous compact result)
     * - prev.wal (rotated WAL-file)
     * - current.wal (current WAL-file [inactive], should be used only in recovery-mod)
     * <p>
     * Since compact can used for large files we have to use external sort and temp dir.
     * As the result "compacted" file replaced by new one.
     */
    static void CompactWal(WalConfig cfg, boolean with_current) throws IOException {
        WalCompactor compactor = new WalCompactor(cfg);
        ArrayList<File> files = new ArrayList<>();
        boolean used_current_wal = false;

        if (!cfg.getCompactingTmpDir().exists() && !cfg.getCompactingTmpDir().mkdir())
            throw new IOException("Can't create compacting directory");

        if (cfg.getPreviousWalPath().exists())
            files.add(cfg.getPreviousWalPath());

        if (cfg.getCompactedWalPath().exists())
            files.add(cfg.getCompactedWalPath());

        // should be used in recovery
        if (with_current && cfg.getCurrentWalPath().exists()) {
            files.add(cfg.getCurrentWalPath());
            used_current_wal = true;
        }

        compactor.Compact(files, cfg.getCompactedTmpWalPath());

        if (used_current_wal)
            removeWithException(cfg.getCurrentWalPath());

        removeWithException(cfg.getPreviousWalPath());
        removeWithException(cfg.getCompactedWalPath());

        moveWithException(cfg.getCompactedTmpWalPath(), cfg.getCompactedWalPath());
        FileUtils.deleteDirectory(cfg.getCompactingTmpDir());
    }

    static public void removeCompactedWal(WalConfig cfg) throws IOException {
        removeWithException(cfg.getCompactedWalPath());
    }

    private void reopenWal() throws IOException {
        writer = new WalWriter(cfg.getCurrentWalPath());
        stat.reset();
    }

    private void rotateWal() throws IOException {
        writer.close();
        removeWithException(cfg.getPreviousWalPath());
        moveWithException(cfg.getCurrentWalPath(), cfg.getPreviousWalPath());
        reopenWal();
    }

    private void launchCompact() throws IOException {
        try {
            if (cthread != null)
                cthread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        rotateWal();
        compact_running = true;
        cthread = new CompactorThread();
        cthread.start();
    }

    /**
     * Switch running compact only if we have enough bytes and
     * we can free something (ratio)
     *
     * @throws IOException if there is a problem with launching compact
     */
    private void launchCompactIfNeeds() throws IOException {
        if (!compact_running && stat.nbytes >= cfg.compact_threshold_bytes) {
            float ratio = (float) stat.nrems / stat.nadds;
            if (ratio >= cfg.compact_threshold_ratio) {
                launchCompact();
            }
        }
    }

    /**
     * Force compact to run
     *
     * @return false if compact already running
     * @throws IOException in cases where launchCompactIfNeeds throws it
     */
    public boolean forceCompact() throws IOException {
        if (compact_running)
            return false;

        launchCompact();
        return true;
    }

    /**
     * Add record to WAL
     *
     * @param key  (it will be used to remove data too)
     * @param data user-data
     * @throws IOException in case of IO problem
     */
    public void add(String key, byte[] data) throws IOException {
        stat.account_add(writer.add(key, data));
        launchCompactIfNeeds();
    }

    /**
     * Mark event with param key as finished (remove pair start event)
     *
     * @param key identifier (same as add)
     * @throws IOException in case of IO problem
     */
    public void remove(String key) throws IOException {
        stat.account_remove(writer.remove(key));
        launchCompactIfNeeds();
    }

    /**
     * Wait compact to finish. Should be used only for tests.
     *
     * @throws InterruptedException if thread interrupted
     */
    public void waitCompact() throws InterruptedException {
        if (compact_running)
            cthread.join();
    }

    @Override
    public void close() throws IOException {
        if (writer != null)
            writer.close();
    }

    private class CompactorThread extends Thread {
        @Override
        public void run() {
            try {
                CompactWal(cfg, false);
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                compact_running = false;
            }
        }
    }

    private class statistics {
        int nbytes;
        int nadds;
        int nrems;

        statistics() {
            reset();
        }

        void reset() {
            nbytes = 0;
            nadds = 0;
            nrems = 0;
        }

        void account_add(int size) {
            nadds++;
            nbytes += size;
        }

        void account_remove(int size) {
            nrems++;
            nbytes += size;
        }
    }
}
