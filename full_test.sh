#!/usr/bin/env bash

set -e
set -o pipefail

NLOOPS=10
NURLS=1000000
DEMO_DIR=/tmp/wal-demo

mkdir -p $DEMO_DIR
rm -rf $DEMO_DIR/*

./generate_urls.py --loops $NLOOPS $NURLS | ./build/install/wal_demo/bin/wal_demo.sh

# now recover URLS
./build/install/wal_demo/bin/wal_demo.sh </dev/null | perl -ne 'if (/site-(\d+)/) { print "$1\n" }' | sort -n >$DEMO_DIR/recovered

# generate_urls.py removes first 100 URLS from each loop
for (( loop=0; loop < $NLOOPS; loop++ )); do
    for (( i=0; i < 100; i++ )); do
        echo $[ $loop * $NURLS + $i ]
    done
done >$DEMO_DIR/expected

diff $DEMO_DIR/expected $DEMO_DIR/recovered
echo SUCCESS
